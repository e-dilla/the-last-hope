﻿// (c) Copyright Cleverous 2015. All rights reserved.

using UnityEngine;
using UnityEditor;
using Deftly;

[CustomEditor(typeof(PlayerController))]
public class E_PlayerController : Editor
{
    private PlayerController _x;
    
    // Basic Stats
    private readonly GUIContent _aimControl =       new GUIContent("Aim Control Feel", "How the character aims in response to input");
    private readonly GUIContent _moveControl =      new GUIContent("Move Control Feel", "~~Not working yet.~~");
    private readonly GUIContent _mask =             new GUIContent("Mask", "The layers used in raycasts. Typically just 'floor'");
    private readonly GUIContent _logDebug =         new GUIContent("Log Debug", "Show/Hide debug information");
    private readonly GUIContent _rootMotion =       new GUIContent("Root Motion", "Use root motion data from Mecanim to drive Subject locomotion. Note that your animations must strongly support this.");
    private readonly GUIContent _inControl =        new GUIContent("Use InControl", "Use InControl plugin for peripheral controller support");
    private readonly GUIContent _inControlDevice =  new GUIContent("Device ID", "InControl provides Device IDs, assign them on a per character basis here. Note InControl docs on best practice doing this.");
    private readonly GUIContent _horizontal =       new GUIContent("Horizontal", "Input Manager Name Reference");
    private readonly GUIContent _vertical =         new GUIContent("Vertical", "Input Manager Name Reference");
    private readonly GUIContent _fire1 =            new GUIContent("Fire 1", "Input Manager Name Reference");
    private readonly GUIContent _fire2 =            new GUIContent("Fire 2", "Input Manager Name Reference");
    private readonly GUIContent _reload =           new GUIContent("Reload", "Input Manager Name Reference");
    private readonly GUIContent _interact =         new GUIContent("Interact", "Input Manager Name Reference");
    private readonly GUIContent _dropWeapon =       new GUIContent("Drop Weapon", "Input Manager Name Reference");
    private readonly GUIContent _changeWeapon =     new GUIContent("Change Weapon", "Input Manager Name Reference");
    private readonly GUIContent _walkAnimSpeed =    new GUIContent("Walk Anim Speed", "Input Manager Name Reference");
    private readonly GUIContent _runAnimSpeed =     new GUIContent("Run Anim Speed", "Input Manager Name Reference");
    private readonly GUIContent _animDampening =    new GUIContent("Animator Dampening", "Dampening for the inputs going to the animator. Might help with jerky animations.");
    
    void OnEnable()
    {
        _x = (PlayerController)target;
    }

    public override void OnInspectorGUI()
    {
        GUI.changed = false;
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        if (GUILayout.Button("General", EditorStyles.toolbarButton)) EditorUtils.PlayControlsGeneral = !EditorUtils.PlayControlsGeneral;
        if (EditorUtils.PlayControlsGeneral) ShowGeneral();

        if (GUILayout.Button("Input References", EditorStyles.toolbarButton)) EditorUtils.PlayControlsLabels = !EditorUtils.PlayControlsLabels;
        if (EditorUtils.PlayControlsLabels) ShowLabels();

        if (GUILayout.Button("Peripheral Config", EditorStyles.toolbarButton)) EditorUtils.PlayControlsPeriph = !EditorUtils.PlayControlsPeriph;
        if (EditorUtils.PlayControlsPeriph) ShowControllerConfig();

        EditorGUILayout.Space();
        
        Save();
    }

    private void Save()
    {
        serializedObject.ApplyModifiedProperties();
        if (GUI.changed) EditorUtility.SetDirty(_x);
    }
    private void ShowGeneral()
    {
        EditorGUILayout.Space();

        _x.AimControlFeel = (PlayerController.ControlFeel)EditorGUILayout.EnumPopup(_aimControl, _x.AimControlFeel);
        EditorGUI.BeginDisabledGroup(true);
        _x.MoveControlFeel = (PlayerController.ControlFeel)EditorGUILayout.EnumPopup(_moveControl, _x.MoveControlFeel);
        EditorGUI.EndDisabledGroup();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("Mask"), _mask, false);

        EditorGUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = 70;
        _x.LogDebug = EditorGUILayout.Toggle(_logDebug, _x.LogDebug);
        _x.UseRootMotion = EditorGUILayout.Toggle(_rootMotion, _x.UseRootMotion);
        EditorGUIUtility.labelWidth = 110;
        EditorGUILayout.EndHorizontal();

        _x.AnimatorDampening = EditorGUILayout.Slider(_animDampening, _x.AnimatorDampening, 0, 1);
        EditorGUI.BeginDisabledGroup(true);
        _x.RunAnimSpeed = EditorGUILayout.Slider(_runAnimSpeed, _x.RunAnimSpeed, 0, 3);
        _x.WalkAnimSpeed = EditorGUILayout.Slider(_walkAnimSpeed, _x.WalkAnimSpeed, 0, 3);
        EditorGUI.EndDisabledGroup();

        EditorGUILayout.Space();
    }
    private void ShowLabels()
    {
        EditorGUILayout.Space();

        EditorGUILayout.HelpBox("These names correspond to the Inputs in the Input Manager.", MessageType.None);

        _x.Horizontal = EditorGUILayout.TextField(_horizontal, _x.Horizontal);
        _x.Vertical = EditorGUILayout.TextField(_vertical, _x.Vertical);
        _x.Fire1 = EditorGUILayout.TextField(_fire1, _x.Fire1);
        _x.Fire2 = EditorGUILayout.TextField(_fire2, _x.Fire2);
        _x.Reload = EditorGUILayout.TextField(_reload, _x.Reload);
        _x.Interact = EditorGUILayout.TextField(_interact, _x.Interact);
        _x.DropWeapon = EditorGUILayout.TextField(_dropWeapon, _x.DropWeapon);
        _x.ChangeWeapon = EditorGUILayout.TextField(_changeWeapon, _x.ChangeWeapon);

        EditorGUILayout.Space();
    }
    private void ShowControllerConfig()
    {
        EditorGUILayout.HelpBox("InControl must be imported for these settings apply.", MessageType.None);

        EditorGUILayout.Space();

        _x.UseInControl = EditorGUILayout.Toggle(_inControl, _x.UseInControl);
        _x.DeviceNumber = EditorGUILayout.IntField(_inControlDevice, _x.DeviceNumber);

        EditorGUILayout.Space();
    }
}