﻿// (c) Copyright Cleverous 2015. All rights reserved.

using UnityEngine;

namespace Deftly
{
    [AddComponentMenu("Deftly/Player Controller")]
    [RequireComponent(typeof(Subject))]
    public class PlayerController : MonoBehaviour
    {
        public enum ControlFeel { Stiff, Loose }
        public ControlFeel AimControlFeel = ControlFeel.Loose;
        public ControlFeel MoveControlFeel = ControlFeel.Loose;

        [HideInInspector]
        public float AimTension;
        [HideInInspector]
        public float MoveTension;

        public string ChangeWeapon;
        public string Horizontal;
        public string Vertical;
        public string Fire1;
        public string Fire2;
        public string Reload;
        public string Interact;
        public string DropWeapon;
        public LayerMask Mask;
        public bool LogDebug;
        public float WalkAnimSpeed;
        public float RunAnimSpeed;
        public bool UseRootMotion;
        public float AnimatorDampening;

        private Animator _animator;
        private Camera _cam;
        private GameObject _go;
        private Subject _subject;
        private Rigidbody _rb;
        private string _h;
        private string _v;
        private Vector3 _aimInput;
        private Vector3 _aimCache;
        private Vector3 _moveCache;

        // InControl Support
        //
        public bool UseInControl;
        //public InputDevice Device;
        public int DeviceNumber;

        [HideInInspector]
        public bool InputPermission;

        void Reset()
        {
            InputPermission = false;
            AimTension = 20;
            MoveTension = 8;
            WalkAnimSpeed = 1f;
            RunAnimSpeed = 2f;
            ChangeWeapon = "Mouse ScrollWheel";
            Horizontal = "Horizontal";
            Vertical = "Vertical";
            Fire1 = "Fire1";
            Fire2 = "Fire2";
            Reload = "Reload";
            Interact = "Interact";
            DropWeapon = "DropWeapon";
            Mask = -1;
        }
        void Start()
        {
            InputPermission = true;

            // InControl support is setup for single controller right now.
            //
            //if (UseInControl) Device = InputManager.Devices[DeviceNumber];
            _aimCache = Vector3.forward;

            _go = gameObject;
            _subject = GetComponent<Subject>();
            _rb = GetComponent<Rigidbody>();
            _cam = Camera.main;

            if (!_cam) Debug.LogError("Main Camera not found! You must tag your primary camera as Main Camera.");
            if (LogDebug) Debug.Log("Rigidbody: " + _rb);
            if (LogDebug) Debug.Log("Main Camera: " + _cam);
            if (LogDebug) Debug.Log("Subject: " + _subject);

            _h = _subject.ControlStats.AnimatorHorizontal;
            _v = _subject.ControlStats.AnimatorVertical;

            if (_subject.Stats.UseMecanim)
            {
                _animator = _subject.GetAnimator();
                if (LogDebug) Debug.Log("PlayerController: Grabbed Animator from Subject: " + _animator);
                if (_animator == null) Debug.LogWarning("PlayerController: No Animator found! Check the reference to the Animator Host Obj and confirm that it has an Animator component.");
            }
        }

        void FixedUpdate()
        {
            if (_subject.IsDead | !InputPermission) return;
            //if (UseInControl && Device == null) Device = InputManager.Devices[DeviceNumber];

            Aim();
            Move();
        }

        void Aim()
        {
            Ray ray = new Ray();

            _aimInput = GetAimAxis;
            if (_aimInput != Vector3.zero) _aimCache = _aimInput;
            else _aimInput = _aimCache;

            // Find the Aim Point depending on input type
            if (UseInControl)
            {
                float halfWidth = Screen.width/2f;
                float halfHeight = Screen.height/2f;
                _aimInput = new Vector3(
                    halfWidth + (_aimInput.x*halfWidth),
                    halfHeight + (_aimInput.z*halfHeight),
                    0);
                ray = _cam.ScreenPointToRay(_aimInput);
            }
            else ray = _cam.ScreenPointToRay(_aimInput);

            // Raycast into the scene
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000, Mask))
            {
                Vector3 dir = hit.point - _go.transform.position; dir.y = 0f;
                Quaternion fin = Quaternion.LookRotation(dir);

                switch (AimControlFeel)
                {
                    // Stiff can have a maximum turn rate.
                    case ControlFeel.Stiff:
                        float angle = Quaternion.Angle(_go.transform.rotation, fin);
                        float derp = angle / _subject.ControlStats.TurnSpeed / AimTension;
                        float progress = Mathf.Min(1f, Time.deltaTime / derp);

                        _go.transform.rotation = Quaternion.Slerp
                            (transform.rotation, fin, progress);
                        break;

                    // Loose is a standard smooth blend. No max turn rate can be established.
                    case ControlFeel.Loose:
                        _go.transform.rotation = Quaternion.Slerp
                            (_go.transform.rotation, fin, Time.deltaTime * _subject.ControlStats.TurnSpeed);
                        break;
                }
            }

            if (LogDebug)
            {
                Debug.DrawLine(gameObject.transform.position, hit.point, Color.red);
                Debug.DrawLine(_cam.transform.position, hit.point, Color.green);
            }
        }
        void Move()
        {

            // COMMENT:
            // Yeah it's a little weird and obscure, but it works great and is super inexpensive to process so I don't care.


            // Get the raw input.
            Vector3 input = GetMovementAxis;

            // Store original camera Quaternion.
            Quaternion s = _cam.transform.rotation;

            // Get the Camera Euler rotation, then flatten the X rotation.
            // The X rotation screws up the TransformDirection() below, so we set it to 0.
            Vector3 t = _cam.transform.rotation.eulerAngles;
            t.x = 0;

            // Apply the fixed rotation, now TransformDirection works correctly, then apply/revert to the original stored rotation.
            _cam.transform.rotation = Quaternion.Euler(t);
            Vector3 movement = _cam.transform.TransformDirection(input);
            _cam.transform.rotation = s;

            // Get data for Mecanim which will be the translated inputs (movement) but relative to the body's (Subject's) facing direction.
            Vector3 relative = _subject.transform.InverseTransformDirection(movement);

            // And now the movement and mecanim translation should be bang-on accurate no matter where the camera is.




            // Apply the movement to the Subject
            _rb.MovePosition(transform.position + movement * _subject.ControlStats.MoveSpeed * .01f);
            
            // Apply the relative to Mecanim
            if (_subject.Stats.UseMecanim)
            {
                if (!_animator)
                {
                    Debug.LogWarning("No Animator Component was found so the PlayerController cannot animate the Subject. Have you assigned the Animator Host Obj?");
                }
                else
                {
                    if (AnimatorDampening > 0.01)
                    {
                        _animator.SetFloat(_h, relative.x, AnimatorDampening, Time.deltaTime);
                        _animator.SetFloat(_v, relative.z, AnimatorDampening, Time.deltaTime);
                    }
                    else
                    {
                        _animator.SetFloat(_h, relative.x);
                        _animator.SetFloat(_v, relative.z);
                    }

                }
            }
        }

        public Vector3 GetMovementAxis { get { return new Vector3(Input.GetAxis(Horizontal), 0, Input.GetAxis(Vertical)); } }
        public Vector3 GetAimAxis { get { return Input.mousePosition; } }
        public float GetInputFire1 { get { return Input.GetAxis(Fire1); } }
        public float GetInputFire2 { get { return Input.GetAxis(Fire2); } }
        public bool GetInputInteract { get { return Input.GetButton(Interact); } }
        public bool GetInputDropWeapon { get { return Input.GetButton(DropWeapon); } }
        public bool GetInputReload { get { return Input.GetButton(Reload); } }
        public float InputChangeWeapon { get { return Input.GetAxisRaw(ChangeWeapon); } }

        public void ResetMecanimParameters()
        {
            if (_animator)
            {
                _animator.SetFloat(_h, 0f);
                _animator.SetFloat(_v, 0f);
            }
        }
    }
}