﻿using UnityEngine;

public class LightFlicker : MonoBehaviour {

	public int steps;
	public float intensityMultiplier;

    private Light lightToFlicker;
    private float[] smoothing;


	// Use this for initialization
	void Start () {
        lightToFlicker = GetComponent<Light>();
		smoothing = new float[steps];

		for(int i = 1; i<smoothing.Length; i++){
			smoothing[i] = 0.0f;
		}
	
	}
	
	// Update is called once per frame
	void Update () {

		float sum = .0f;
		
		// Shift values in the table so that the new one is at the
		// end and the older one is deleted.
		for(int i = 1 ; i < smoothing.Length ; i++)
		{
			smoothing[i-1] = smoothing[i];
			sum+= smoothing[i-1];
		}
		
		// Add the new value at the end of the array.
		smoothing[smoothing.Length -1] = Random.value;
		sum+= smoothing[smoothing.Length -1];
		
		// Compute the average of the array and assign it to the
		// light intensity.
		lightToFlicker.intensity = (sum / smoothing.Length) * intensityMultiplier;

	}
}
