﻿using UnityEngine;
using System.Collections;

public class FloatingBob : MonoBehaviour {

	public float speed;
	public float randomMin;
	public float randomMax;
    public Vector3 offset;
	
	private Vector3 moveTo;
	private Vector3 from;
	private Vector3 to;

	void Start() {

		//Vector3 offset = Vector3.up;
		from = transform.position;
		to = transform.position + offset;
		speed += Random.Range(randomMin, randomMax);

	}


	// Update is called once per frame
	void Update () {

		if(transform.position == from){
			moveTo = to;
		}
		if(transform.position == to){
			moveTo = from;
		}
		transform.position = Vector3.MoveTowards(transform.position, moveTo, speed*Time.deltaTime);

	}




}
