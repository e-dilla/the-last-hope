﻿/*
 * By Chance Studio internal codebank, not to be distributed
 */

using UnityEngine;

namespace ByChanceLibrary {

    /// <summary>
    /// Static classes that contain no identifying code (transform.position, transform.rotation, etc)
    /// </summary>
    public static class BCL {

        /// <summary>
        /// Runs a scene tag check
        /// </summary>
        /// <param name="name">Tag to look for</param>
        /// <returns>True if that tag is found : False if tag is not found</returns>
        public static bool CheckTag(string name) {
            return GameObject.FindGameObjectsWithTag(name).Length > 0;
        }

        /// <summary>
        /// Toggles GameObject
        /// </summary>
        /// <param name="obj">Object to toggle</param>
        public static void ToggleActive(GameObject obj) {
            obj.SetActive(!obj.activeSelf);
        }


        /// <summary>
        /// Checks for a game object tag in a CapsuleCast
        /// </summary>
        /// <param name="tag">Tag of the object to look for</param>
        /// <param name="castPosition">Position to start the cast</param>
        /// <param name="range">Range of the raycast</param>
        /// <param name="radius">Radius of the capsule during the raycast</param>
        /// <returns></returns>
        public static GameObject CheckForObject(string tag, Transform castPosition, float range, float radius) {
            RaycastHit hit;
            Vector3 startPos = castPosition.position;
            Vector3 endPos = startPos + castPosition.forward * 0.6f;

            if (Physics.CapsuleCast(startPos, endPos, radius, castPosition.forward, out hit, range)) {
                if (hit.transform.gameObject.tag == tag) {
                    return hit.transform.gameObject;
                }
            }
            return null;
        }



    }


}
