using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelSelectScript : MonoBehaviour {

    public GameObject loadingText;
	
    // Use this for initialization
	void Start () {
        loadingText.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Schoolzone()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(3);
    }

    public void Infirmaryzone()
    {
        Time.timeScale = 1;
        loadingText.SetActive(true);
        SceneManager.LoadScene(2);
    }

    public void Back()
    {
        SceneManager.LoadScene(0);
    }
}
