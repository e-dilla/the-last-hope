using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;

public class FBScript : MonoBehaviour
{
    public GameObject LoggedInButton;
    public GameObject LoggedOutButton;
    public string Message;

    void Awake()
    {
        FB.Init(SetInit, onHideUnity);
    }

    void SetInit()
    {
        if (FB.IsLoggedIn)
        {
            Debug.Log("FB is logged in");
        }
        else
        {
            Debug.Log("FB is not logged in");
        }
    }

    void onHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    public void FBlogin()
    {
        List<string> permissions = new List<string>();
        permissions.Add("public_profile");

        FB.LogInWithReadPermissions(permissions, AuthCallBack);
    }

    void AuthCallBack(IResult result)
    {
        if (result.Error != null)
        {
            Debug.Log(result.Error);
        }
        else
        {
            if (FB.IsLoggedIn)
            {
                Debug.Log("FB is logged in");
            }
            else
            {
                Debug.Log("FB is not logged in");
            }
        }
    }

    public void Share()
    {
        FB.FeedShare (
            string.Empty,
            new System.Uri("http://linktoga.me"),
            "Can you beat My Score",
            "Score: " + GameManagerScript.score,
            "Check it out",
            new System.Uri("http://uimg.ngfiles.com/profile/5743/5743271.png"),
            string.Empty,
            ShareCallback);
    }

    public void FBScore()
    {
        FB.FeedShare(
            string.Empty,
            new System.Uri("http://linktoga.me"),
            "The Last Hope",
            "Save Us",
            "Sci Fi Shooter",
            new System.Uri("http://uimg.ngfiles.com/profile/5743/5743271.png"),
            string.Empty,
            ShareCallback);
    }

    void ShareCallback(IResult result)
    {
        if (result.Cancelled)
        {
            Debug.Log("Share Cancelled");
        }
        else if (!string.IsNullOrEmpty(result.Error))
        {
            Debug.Log("Error on share!");
        }
        else if (!string.IsNullOrEmpty(result.RawResult))
        {
            Debug.Log("Success on share");
        }
    }

    public void TwitterShare()
    {
        Application.OpenURL("https://twitter.com/intent/tweet?text" +Message +"&amp;lang=eng");
    }

}
