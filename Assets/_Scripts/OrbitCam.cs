﻿using UnityEngine;
using System.Collections;

public class OrbitCam : MonoBehaviour {

    public Transform spot;
    public float speed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(spot);
        transform.Translate(Vector3.right * Time.deltaTime * speed);
    }
}
