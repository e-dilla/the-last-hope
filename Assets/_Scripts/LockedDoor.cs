using UnityEngine;
using System.Collections;

public class LockedDoor : MonoBehaviour {

    public Animator animator;
    public bool doorOpen;

    void Start()
    {
        doorOpen = false;
        animator = GetComponent<Animator>();
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name == "Player" && GameManagerScript.stageCompleted == true)
        {
            doorOpen = true;
            DoorControl("Open");
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (doorOpen && col.gameObject.name == "Player")
        {
            doorOpen = false;
            DoorControl("Close");
        }
    }

    void DoorControl(string direction)
    {
        animator.SetTrigger(direction);
    }

}
