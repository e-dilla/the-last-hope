using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TitleButtonScript : MonoBehaviour {

    public GameObject titleText;
    public GameObject titleButtons;
    public GameObject creditsButtons;
    public GameObject loadingText;
    public GameObject creditsText;
    public GameObject quitButtons;
    public GameObject backButton;
    public GameObject levelSelectButton;

    // Use this for initialization
    void Start () {

        loadingText.SetActive(false);
        creditsText.SetActive(false);
        levelSelectButton.SetActive(false);

    }
	
    public void PlayPress()
    {
        titleButtons.SetActive(false);
        levelSelectButton.SetActive(true);
    }

    public void CreditPress()
    {
        titleButtons.SetActive(false);
        creditsText.SetActive(true);
    }

    public void BackPress()
    {
        creditsText.SetActive(false);
        levelSelectButton.SetActive(false);
        titleButtons.SetActive(true);
    }

    public void Infirmaryzone()
    {
        Time.timeScale = 1;
        loadingText.SetActive(true);
        SceneManager.LoadScene(2);
    }
}
