using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Deftly;

public class GameManagerScript : MonoBehaviour {

    public static int score = 0;
    public static int parts = 0;
    public static int computerNumbers = 0;
    public Text scoreText;
    public Text partsText;
    public Text healthText;
    public Text computerText;
    public static bool hackedComputer = false;
    public static bool destoryedComputer = false;
    public static bool stageCompleted = false;
    public Text hackedText;
    public Text destoryText;
    public Subject player;
    public GameObject youLoseUI;
    public GameObject miniCutSence;


	// Use this for initialization
	void Start () {
        score = 0;
        computerNumbers = 0;
        Time.timeScale = 0;
        youLoseUI.SetActive(false);
        //text = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {

        scoreText.text = "Score: " + score;
        partsText.text = "Parts: " + parts;
        healthText.text = "Health: " + player.Stats.Health.Actual;
        //computerText.text = "" + computerNumbers;
        Computers();
        Computercount();
        StageComplete();

        if(player.Stats.Health.Actual <= 0)
        {
            StartCoroutine(YouDied());
        }

        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }
    
    IEnumerator YouDied()
    {
        yield return new WaitForSeconds(3);
        youLoseUI.SetActive(true);
    }

    void Computers()
    {
        if (hackedComputer)
        {
            hackedText.text = "Computer Hacked";
            computerText.text = "Hacked";
            Debug.Log("Got it");
        }

        if (destoryedComputer)
        {
            destoryText.text = "All Destoryed";
        }
    }

    void StageComplete()
    {
        if (hackedComputer && destoryedComputer)
        {
            stageCompleted = true;
        }
    }

    void Computercount()
    {
        if (computerNumbers == 6)
        {
            destoryedComputer = true;
        }
    }

    public void Back()
    {
        SceneManager.LoadScene(0);
    }

    public void QuitButton()
    {
        Application.Quit();
    }

    public void RetryButton()
    {
        //SceneManager.LoadScene(l)
        Application.LoadLevel(Application.loadedLevel);
    }

    public void CutSenceDisabler()
    {
        miniCutSence.SetActive(false);
        Time.timeScale = 1;
    }


}
