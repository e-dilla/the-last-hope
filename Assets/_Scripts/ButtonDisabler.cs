using UnityEngine;
using System.Collections;

public class ButtonDisabler : MonoBehaviour {

    public GameObject Buttons;

	// Use this for initialization
	void Awake () {
        Buttons.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {

        if (Time.timeScale != 0)
        {
            Buttons.SetActive(true);
        }
	}
}
