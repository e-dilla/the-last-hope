﻿// (c) Copyright Cleverous 2015. All rights reserved.

using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;


namespace Deftly
{
    [AddComponentMenu("Deftly/Player Controller")]
    [RequireComponent(typeof(Subject))]
    public class PlayerController : MonoBehaviour
    {
        public enum ControlFeel { Stiff, Loose }
        public ControlFeel AimControlFeel = ControlFeel.Loose;
        public ControlFeel MoveControlFeel = ControlFeel.Loose;
        
        [HideInInspector]
        public float AimTension;
        [HideInInspector]
        public float MoveTension;

        public string ChangeWeapon;
        public string Horizontal;
        public string HorizontalRight;
        public string Vertical;
        public string VerticalRight;
        public string Fire1;
        public string Fire2;
        public string Reload;
        public string Interact;
        public string DropWeapon;
        public LayerMask Mask;
        public bool LogDebug;
        public float WalkAnimSpeed;
        public float RunAnimSpeed;

        private Animator _animator;
        private Camera _cam;
        private GameObject _go;
        private Subject _subject;
        private Rigidbody _rb;
        private string _h;
        private string _v;
        private GameObject _arbiter;
        private Vector3 _aimInput;
        private Vector3 _aimCache;

        // InControl Support
        //
        public bool UseInControl;
        public float Device;
        public int DeviceNumber;

        [HideInInspector]
        public bool InputPermission;

        void Reset()
        {
            InputPermission = false;
            AimTension = 20;
            MoveTension = 8;
            WalkAnimSpeed = 1f;
            RunAnimSpeed = 2f;
            ChangeWeapon = "Mouse ScrollWheel";
            Horizontal = "Horizontal";
            Horizontal = "HorizontalRight";
            Vertical = "Vertical";
            Vertical = "VerticalRight";
            Fire1 = "Fire1";
            Fire2 = "Fire2";
            Reload = "Reload";
            Interact = "Interact";
            DropWeapon = "DropWeapon";
            Mask = -1;
        }
        void Start()
        {
            InputPermission = true;

            // InControl is setup for single controller right now.
            //
            //if (UseInControl) Device = InputManager.Devices[DeviceNumber];
            _aimCache = Vector3.forward;

            _go = gameObject;
            _subject = GetComponent<Subject>();
            _rb = GetComponent<Rigidbody>();
            _cam = Camera.main;

            if (LogDebug) Debug.Log("Rigidbody: " + _rb);
            if (LogDebug) Debug.Log("Main Camera: " + _cam);
            if (_cam != null) _arbiter = _cam.GetComponent<DeftlyCamera>().GetArbiter();
            else Debug.LogError("Main Camera not found! You must tag your primary camera as Main Camera.");

            if (LogDebug) Debug.Log("Subject: " + _subject);

            _h = _subject.ControlStats.AnimatorHorizontal;
            _v = _subject.ControlStats.AnimatorVertical;

            if (_subject.Stats.UseMecanim)
            {
                _animator = _subject.GetAnimator();
                if (LogDebug) Debug.Log("PlayerController: Grabbed Animator from Subject: " + _animator); 
                if (_animator == null) Debug.LogWarning("PlayerController: No Animator found! Check the reference to the Animator Host Obj and confirm that it has an Animator component.");
            }
        }

        void FixedUpdate()
        {
            if (_subject.IsDead | !InputPermission) return;
            //if (UseInControl && Device == null) Device = InputManager.Devices[DeviceNumber];

            Aim();
            Move();
        }

        void Aim()
        {
            Ray ray = new Ray();

            _aimInput = GetAimAxis;
            if (_aimInput != Vector3.zero) _aimCache = _aimInput;
            else _aimInput = _aimCache;

            if (UseInControl)
            {
                _aimInput = _arbiter.transform.TransformDirection(_aimInput);
                Vector3 spot = transform.position + _aimInput;
                ray.origin = _cam.transform.position;
                ray.direction = spot - ray.origin;
            }
            else ray = _cam.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000, Mask))
            {
                Vector3 dir = hit.point - _go.transform.position; dir.y = 0f;
                Quaternion fin = Quaternion.LookRotation(dir);

                switch (AimControlFeel)
                {
                        // Stiff can have a maximum turn rate.
                    case ControlFeel.Stiff:
                        float angle = Quaternion.Angle(_go.transform.rotation, fin);
                        float derp = angle / _subject.ControlStats.TurnSpeed / AimTension;
                        float progress = Mathf.Min(1f, Time.deltaTime / derp);

                        _go.transform.rotation = Quaternion.Slerp
                            (transform.rotation, fin, progress);
                        break;

                        // Loose is a standard smooth blend. No max turn rate can be established.
                    case ControlFeel.Loose:
                        _go.transform.rotation = Quaternion.Slerp
                            (_go.transform.rotation, fin, Time.deltaTime * _subject.ControlStats.TurnSpeed);
                        break;
                }
            }

            if (LogDebug)
            {
                Debug.DrawLine(gameObject.transform.position, hit.point, Color.red);
                Debug.DrawLine(_cam.transform.position, hit.point, Color.green);
            }
        }
        void Move()
        {
            // The camera can be at any angle, so its inaccurate to use TransformDirection directly.
            // Soooo... we have an Arbiter Object above the average position with a corrected angle.
            // Once I find a better way to solve the transform formula i'll remove the arbiter and simplify this.

            // This is the input after including control variables.
            Vector3 input = GetMovementAxis;

            // This corrects the movement direction to be relative to the camera angle and applies it.
            Vector3 movement = _arbiter.transform.TransformDirection(input);

            // Now use the result to move the player
            _rb.MovePosition(_go.transform.position + movement*.01f);

            // Clearly i suffer from insanity
            Vector3 relative = _subject.transform.InverseTransformDirection(movement);

            if (_subject.Stats.UseMecanim)
            {
                if (!_animator)
                {
                    Debug.LogWarning("No Animator Component was found so the PlayerController cannot animate the Subject. Have you assigned the Animator Host Obj?");
                }
                else
                {
                    _animator.SetFloat(_h, relative.x);
                    _animator.SetFloat(_v, relative.z);
                }
            }
        }

        // Left Stick Or keyboard axis of movement
        //
        public Vector3 GetMovementAxis 
        {
            get
            {
                return UseInControl
                    ? new Vector3(
                        0, 0, 0)
                        //Device.LeftStick.X *_subject.ControlStats.MoveSpeed, 
                        //0, 
                        //Device.LeftStick.Y *_subject.ControlStats.MoveSpeed)
                    : new Vector3(
                        CrossPlatformInputManager.GetAxis(Horizontal) *_subject.ControlStats.MoveSpeed,
                        0, 
                        CrossPlatformInputManager.GetAxis(Vertical) *_subject.ControlStats.MoveSpeed);
            }
        }

        // Right Stick or mouse screen input
        //
        public Vector3 GetAimAxis { get { return UseInControl ? new Vector3(Device, 0, Device) : new Vector3(CrossPlatformInputManager.GetAxis(HorizontalRight),0, CrossPlatformInputManager.GetAxis(VerticalRight)); }}

        // L2 and R2 Trigger Buttons
        //
        public float GetInputFire1 { get { return (CrossPlatformInputManager.GetAxis(HorizontalRight) + CrossPlatformInputManager.GetAxis(VerticalRight)); } }
        public float GetInputFire2 { get { return UseInControl ? Device : Input.GetAxis(Fire2); } }

        // Action Buttons
        //
        public bool GetInputInteract { get { return UseInControl ? Input.GetButton(Interact) : Input.GetButton(Interact); } }
        public bool GetInputDropWeapon { get { return UseInControl ? Input.GetButton(Interact) : Input.GetButton(DropWeapon); } }
        public bool GetInputReload { get { return UseInControl ? Input.GetButton(Interact) : Input.GetButton(Reload); } }

        // DPad Left and Right
        //
        public float InputChangeWeapon { 
            get 
            {
                if (!UseInControl) return Input.GetAxisRaw(ChangeWeapon);
                //if (Device.DPadLeft != 0) return -1f;
                //if (Device.DPadRight != 0) return 1f;
                return 0f;
            }
        }

        void OnGUI()
        {
            if (UseInControl)// && Device != null)
            {
                //GUILayout.Label(Device.Name);
                GUILayout.Label("Movement Axis: " + GetMovementAxis);
                GUILayout.Label("Aim Axis: " + GetAimAxis);
                GUILayout.Label("Swap Input: " + InputChangeWeapon);
            }
        }

        public void ResetMecanimParameters()
        {
            if (_animator)
            {
                _animator.SetFloat(_h, 0f);
                _animator.SetFloat(_v, 0f);
            }
        }
    }
}